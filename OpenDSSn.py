"""
Library that implements basic OpenDSS objects

class OpenDSSBasic contains methods that return OpenDSS objects and also implements method that
returns full Y matrix of a system based on passed flag (must be included in 'flags' list
"""

import win32com.client


class OpenDSSBasic:
    __flags = ["POS_SEQ_NO_LOAD", "POS_SEQ_WITH_LOAD", "NO_LOAD", "WITH_LOAD", "NO_LaG", "POS_SEQ_NO_LaG"]

    def __init__(self, dss_script):
        """Class constructor

        Args:
            dss_script (string): Apsolute path to OpenDss script

        Examples:
            >>> dss_script = r'c:\Users\Varga\PycharmProjects\OpenDSS-IEEE13bus\IEEE13Nodeckt.dss'
            >>> dssObject = OpenDSSBasic(dss_script)

        """
        self.__dss_script_full_path = dss_script

        self.__dssObj = win32com.client.Dispatch("OpenDSSEngine.DSS")
        self.__dssText = self.__dssObj.Text
        self.__dssCircuit = self.__dssObj.ActiveCircuit
        self.__dssSolution = self.__dssCircuit.Solution
        self.__dssElem = self.__dssCircuit.ActiveCktElement
        self.__dssBus = self.__dssCircuit.ActiveBus

    def get_dss_obj(self):
        return self.__dssObj

    def get_dss_text(self):
        return self.__dssText

    def get_dss_circuit(self):
        return self.__dssCircuit

    def get_dss_solution(self):
        return self.__dssSolution

    def get_dss_elem(self):
        return self.__dssElem

    def get_dss_bus(self):
        return self.__dssBus

    def get_dss_busN(self):
            return self.__dssCircuit.AllNodeNames, self.__dssCircuit.AllNodeNames

    def get_flags(self):
            return self.__flags

    def get_y_matrix(self, flag):
        """Method that calculates system Y matrix based on a flag

        Args:
            flag (string): One of available flags from flags[] based on which matrix is desired

        Examples:
            >>> dss_script = r'c:\Users\Varga\PycharmProjects\OpenDSS-IEEE13bus\IEEE13Nodeckt.dss'
            >>> dssObject = OpenDSSBasic(dss_script)
            >>> matrix = dssObject.get_y_matrix("WITH_LOAD")

        Returns:
            tuple: returns one dimensional tuple with all calculated matrix members

        """
        assert flag in self.__flags, "'" + flag + "' is not a valid flag. Please use one of these:\n" + str(flags)

        dss_script = open(self.__dss_script_full_path, 'r')
        script_lines = list(dss_script)
        dss_script.close()

        new_script = open(self.__dss_script_full_path, "w")
        is_makeposseq_written = False

        if flag == "WITH_LOAD":
            for line in script_lines:
                if "redirect loads.dss" in str(line).lower():
                    line = "redirect loads.dss\n"
                if "makeposseq" in str(line).lower():
                    line = ""
                new_script.write(line)

        elif flag == "NO_LOAD":
            for line in script_lines:
                if "redirect loads.dss" in str(line).lower():
                    line = "!redirect loads.dss\n"
                if "makeposseq" in str(line).lower():
                    line = ""
                new_script.write(line)

        elif flag == "POS_SEQ_WITH_LOAD":
            for line in script_lines:
                if "makeposseq" in str(line).lower():
                    is_makeposseq_written = True

            for line in script_lines:
                if "redirect loads.dss" in str(line).lower():
                    line = "redirect loads.dss\n"
                if "solve" in str(line).lower() and not is_makeposseq_written:
                    line = "makeposseq\nsolve\n"
                new_script.write(line)

        elif flag == "POS_SEQ_NO_LOAD":
            for line in script_lines:
                if "makeposseq" in str(line).lower():
                    is_makeposseq_written = True

            for line in script_lines:
                if "redirect loads.dss" in str(line).lower():
                    line = "!redirect loads.dss\n"
                if "solve" in str(line).lower() and not is_makeposseq_written:
                    line = "makeposseq\nsolve\n"
                new_script.write(line)

        elif flag == "NO_LaG":
            for line in script_lines:
                if "redirect loads.dss" in str(line).lower():
                    line = "!redirect loads.dss\n"
                if "vsource.source.enabled=no" in str(line).lower():
                    line = "vsource.source.enabled=no\n"
                if "makeposseq" in str(line).lower():
                    line = ""
                new_script.write(line)

        elif flag == "POS_SEQ_NO_LaG":
            for line in script_lines:
                if "makeposseq" in str(line).lower():
                    is_makeposseq_written = True

            for line in script_lines:
                if "redirect loads.dss" in str(line).lower():
                    line = "!redirect loads.dss\n"
                if "vsource.source.enabled=no" in str(line).lower():
                    line = "vsource.source.enabled=no\n"
                if "solve" in str(line).lower() and not is_makeposseq_written:
                    line = "makeposseq\nsolve\n"
                new_script.write(line)

        new_script.close()

        self.__dssText.Command = "compile '" + self.__dss_script_full_path + "'"
        return self.__dssCircuit.SystemY