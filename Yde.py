# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 15:13:18 2018

@author: Marinko Barukcic, FERIT Osijek
"""
import numpy as np


class Y_dssMatrix:

    def __init__(self, Ym):
        self.__Ym = Ym
        self.__Ymo = int(np.sqrt(len(self.__Ym)/2)) # red matrice Y

    def Y_make(self):
        YmL = []
        for h in range(self.__Ymo):
            YmL.append(self.__Ym[h*(2*self.__Ymo):h*(2*self.__Ymo)+2*self.__Ymo])

        YmM = np.zeros((self.__Ymo,2*self.__Ymo))
        for k in range(len(YmL)):
            YmM[k,:] = YmL[k]

        YmMc = 1j*np.zeros((self.__Ymo,self.__Ymo))
        k1 = 0
        for n in range(len(YmM)):
            k2 = 0
            for m in range(0,len(YmL[0]),2):
                YmMc[k1,k2] = YmM[n,m]+1j*YmM[n,m+1]
                k2 +=1
            k1 +=1

        return YmMc

    def Y_spectr(self, fz):
        Ys = self.Y_make() # numpy [n x n] matrica iz metode Ymake
        Ys = Ys[fz:len(Ys),fz:len(Ys)]
        SvVV = np.linalg.eigh(Ys)
        SvVrj = SvVV[0]
        SvVctD = SvVV[1]
        D = 1/SvVrj

        return SvVV, SvVrj, SvVctD, D

    def Y_sens(self, iLAM, fz):
        SvVV, SvVrj, SvVctD, D = self.Y_spectr(fz)

        Wd = np.matrix(SvVctD) # matrica desnih svojstvenih vektora (V)
        Wl = Wd.getH() # matrica lijevih svojstvenih vektora (W)
        Wdt = Wd.T
        Wlt = Wl.T
        OSJ1 = np.dot(Wlt[:,iLAM], Wdt[iLAM,:])
        OSJ2 = np.dot(Wd[:,iLAM],Wl[iLAM,:])

        return Wd, Wl, OSJ1, OSJ2

